import {
  Card,
  CardBody,
  CardFooter,
  Divider,
  Flex,
  Heading,
  Image,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Slider,
  SliderFilledTrack,
  SliderThumb,
  SliderTrack,
  Stack,
  Text,
  VStack,
} from '@chakra-ui/react'
import { useState } from 'react'
import type { ArmyState, TierTypes } from '~/hooks/useEpicBattle'
import TierChooser from '../TierChooser'
import TierTroopDisplay from '../TierTroopDisplay'

export const griffinStats = {
  leadership: 1,
  tiers: {
    tier1: { strength: 0, health: 0 },
    tier2: { strength: 0, health: 0 },
    tier3: { strength: 0, health: 0 },
    tier4: { strength: 0, health: 0 },
    tier5: { strength: 10000, health: 30000 },
  },
}

interface GriffinProps {
  griffin: ArmyState['battleGriffin']
  setGriffin: (tier: TierTypes, amount: number) => void
}

export default function Griffin({ griffin, setGriffin }: GriffinProps) {
  return (
    <Card maxW="md">
      <CardBody>
        <Image
          src="/images/tb/guardsman/griffin/t5.png"
          alt="Griffin V"
          borderRadius="lg"
          boxSize="200"
        />
        <TierTroopDisplay troops={griffin} />
      </CardBody>
      <Divider />
      <CardFooter>
        <TierChooser min={5}>
          {(tier) => (
            <VStack>
              <Stack mt="6" spacing="3">
                <Heading size="md">Griffin</Heading>
                <Text color="orange.700" fontSize="xl">
                  Strength: {griffinStats.tiers[tier]?.strength}
                </Text>
                <Text color="red.500" fontSize="xl">
                  Health: {griffinStats.tiers[tier]?.health}
                </Text>
              </Stack>

              <Flex>
                <NumberInput
                  maxW="150px"
                  value={griffin[tier]}
                  onChange={(valueAsString: string, valueAsNumber: number) =>
                    setGriffin(tier, valueAsNumber)
                  }
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
                {/* <Slider
                  flex="1"
                  focusThumbOnChange={false}
                  value={spearman[tier]}
                  onChange={(nextValue: number) => setGriffin(tier, nextValue)}
                >
                  <SliderTrack>
                    <SliderFilledTrack />
                  </SliderTrack>
                  <SliderThumb
                    fontSize="sm"
                    boxSize="32px"
                    children={spearman[tier]}
                  />
                </Slider> */}
              </Flex>
            </VStack>
          )}
        </TierChooser>
      </CardFooter>
    </Card>
  )
}
