import {
  Card,
  CardBody,
  CardFooter,
  Divider,
  Flex,
  Heading,
  Image,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Slider,
  SliderFilledTrack,
  SliderThumb,
  SliderTrack,
  Stack,
  Text,
  VStack,
} from '@chakra-ui/react'
import { useState } from 'react'
import type { ArmyState, TierTypes } from '~/hooks/useEpicBattle'
import TierChooser from '../TierChooser'
import TierTroopDisplay from '../TierTroopDisplay'

export const riderStats = {
  leadership: 1,
  tiers: {
    tier1: { strength: 100, health: 300 },
    tier2: { strength: 180, health: 540 },
    tier3: { strength: 320, health: 960 },
    tier4: { strength: 580, health: 1740 },
    tier5: { strength: 1050, health: 3150 },
  },
}

interface RiderProps {
  rider: ArmyState['rider']
  setRider: (tier: TierTypes, amount: number) => void
}

export default function Rider({ rider, setRider }: RiderProps) {
  return (
    <Card maxW="md">
      <CardBody>
        <Image
          src="/images/tb/guardsman/rider/t5.png"
          alt="Rider V"
          borderRadius="lg"
          boxSize="200"
        />
        <TierTroopDisplay troops={rider} />
      </CardBody>
      <Divider />
      <CardFooter>
        <TierChooser>
          {(tier) => (
            <VStack>
              <Stack mt="6" spacing="3">
                <Heading size="md">Rider</Heading>
                <Text color="orange.700" fontSize="xl">
                  Strength: {riderStats.tiers[tier]?.strength}
                </Text>
                <Text color="red.500" fontSize="xl">
                  Health: {riderStats.tiers[tier]?.health}
                </Text>
              </Stack>

              <Flex>
                <NumberInput
                  maxW="150px"
                  value={rider[tier]}
                  onChange={(valueAsString: string, valueAsNumber: number) =>
                    setRider(tier, valueAsNumber)
                  }
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
                {/* <Slider
                  flex="1"
                  focusThumbOnChange={false}
                  value={spearman[tier]}
                  onChange={(nextValue: number) => setRider(tier, nextValue)}
                >
                  <SliderTrack>
                    <SliderFilledTrack />
                  </SliderTrack>
                  <SliderThumb
                    fontSize="sm"
                    boxSize="32px"
                    children={spearman[tier]}
                  />
                </Slider> */}
              </Flex>
            </VStack>
          )}
        </TierChooser>
      </CardFooter>
    </Card>
  )
}
