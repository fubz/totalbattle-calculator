import {
  Card,
  CardBody,
  CardFooter,
  Divider,
  Flex,
  Heading,
  Image,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Slider,
  SliderFilledTrack,
  SliderThumb,
  SliderTrack,
  Stack,
  Text,
  VStack,
} from '@chakra-ui/react'
import { useState } from 'react'
import type { ArmyState, TierTypes } from '~/hooks/useEpicBattle'
import TierChooser from '../TierChooser'
import TierTroopDisplay from '../TierTroopDisplay'

export const spearmanStats = {
  leadership: 1,
  tiers: {
    tier1: { strength: 50, health: 150 },
    tier2: { strength: 90, health: 270 },
    tier3: { strength: 160, health: 480 },
    tier4: { strength: 290, health: 870 },
    tier5: { strength: 520, health: 1560 },
  },
}

interface SpearmanProps {
  spearman: ArmyState['spearman']
  setSpearman: (tier: TierTypes, amount: number) => void
}

export default function Spearman({ spearman, setSpearman }: SpearmanProps) {
  return (
    <Card maxW="md">
      <CardBody>
        <Image
          src="/images/tb/guardsman/spearman/t5.png"
          alt="Spearman V"
          borderRadius="lg"
          boxSize="200"
        />
        <TierTroopDisplay troops={spearman} />
      </CardBody>
      <Divider />
      <CardFooter>
        <TierChooser>
          {(tier) => (
            <VStack>
              <Stack mt="6" spacing="3">
                <Heading size="md">Spearman</Heading>
                <Text color="orange.700" fontSize="xl">
                  Strength: {spearmanStats.tiers[tier]?.strength}
                </Text>
                <Text color="red.500" fontSize="xl">
                  Health: {spearmanStats.tiers[tier]?.health}
                </Text>
              </Stack>

              <Flex>
                <NumberInput
                  maxW="150px"
    
                  value={spearman[tier]}
                  onChange={(valueAsString: string, valueAsNumber: number) =>
                    setSpearman(tier, valueAsNumber)
                  }
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
                {/* <Slider
                  flex="1"
                  focusThumbOnChange={false}
                  value={spearman[tier]}
                  onChange={(nextValue: number) => setSpearman(tier, nextValue)}
                >
                  <SliderTrack>
                    <SliderFilledTrack />
                  </SliderTrack>
                  <SliderThumb
                    fontSize="sm"
                    boxSize="32px"
                    children={spearman[tier]}
                  />
                </Slider> */}
              </Flex>
            </VStack>
          )}
        </TierChooser>
      </CardFooter>
    </Card>
  )
}
