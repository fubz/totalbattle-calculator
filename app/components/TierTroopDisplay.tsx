import {
  Stat,
  StatGroup,
  StatLabel,
  StatNumber,
  VStack,
} from '@chakra-ui/react'
import { useState } from 'react'
import {
  ArcherState,
  BattleGriffenState,
  RiderState,
  SpearmanState,
} from '~/hooks/useEpicBattle'

type TierTroopDisplayProps = {
  // children: (tier: TierTypes) => React.ReactNode
  troops: SpearmanState | ArcherState | RiderState | BattleGriffenState
}

export default function TierTroopDisplay({ troops }: TierTroopDisplayProps) {
  return (
    <StatGroup>
      {Object.entries(troops).map(([tier, amount]) => {
        if (amount > 0) {
          return (
            <Stat key={tier}>
              <StatLabel>{tier}</StatLabel>
              <StatNumber>{amount}</StatNumber>
            </Stat>
          )
        } else {
          return null
        }
      })}
    </StatGroup>
  )
}
