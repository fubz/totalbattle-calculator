import { HStack, Radio, RadioGroup, VStack } from '@chakra-ui/react'
import { useState } from 'react'

type TierChooserProps = {
  children: (tier: TierTypes) => React.ReactNode
  min?: number
}

type TierTypes = 'tier1' | 'tier2' | 'tier3' | 'tier4' | 'tier5'

export default function TierChooser({ children, min }: TierChooserProps) {
  const [tier, setTier] = useState<TierTypes>('tier5')

  return (
    <VStack>
      <RadioGroup
        isDisabled
        onChange={(nextValue) => setTier(nextValue as TierTypes)}
        value={tier}
      >
        <Radio value="tier1">T1</Radio>
        <Radio value="tier2">T2</Radio>
        <Radio value="tier3">T3</Radio>
        <Radio value="tier4">T4</Radio>
        <Radio value="tier5">T5</Radio>
      </RadioGroup>
      {children(tier)}
    </VStack>
  )
}
