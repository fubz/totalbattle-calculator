import React, { useState } from "react";

interface Props {}

const Calculator: React.FC<Props> = () => {
  const [guardsman, setGuardsman] = useState(0);
  const [totalStrength, setTotalStrength] = useState(0);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setTotalStrength(guardsman * 10);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="guardsman">
        Number of Guardsman:
        <input
          type="number"
          id="guardsman"
          value={guardsman}
          onChange={(event) => setGuardsman(+event.target.value)}
        />
      </label>
      <button type="submit">Calculate Total Strength</button>
      <p>Total Strength: {totalStrength}</p>
    </form>
  );
};

export default Calculator;
