import { Link } from '@remix-run/react'
import {
  Box,
  Button,
  Checkbox,
  CheckboxGroup,
  Flex,
  FormControl,
  FormHelperText,
  FormLabel,
  Heading,
  HStack,
  Input,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Radio,
  RadioGroup,
  Spacer,
  Stack,
  Stat,
  StatArrow,
  StatGroup,
  StatHelpText,
  StatLabel,
  StatNumber,
  Text,
  VStack,
} from '@chakra-ui/react'
import Spearman from '~/components/guardsmen/Spearman'
import useEpicBattle from '../hooks/useEpicBattle'
import { useState } from 'react'
import Archer from '~/components/guardsmen/Archer'
import Rider from '~/components/guardsmen/Rider'
import Griffin from '~/components/guardsmen/Griffin'

export default function Index() {
  const {
    army,
    setSpearman,
    setArcher,
    setRider,
    setBattleGriffin,
    optimizeBasedOnLeadership,
    selectedArmy,
    selectGuardsman,
    setSpearmanSelected,
    setArcherSelected,
    setRiderSelected,
    setGriffinSelected,
    setEpicMHSelected,
  } = useEpicBattle()
  const [maxLeadership, setMaxLeadership] = useState(8125)
  const [mercCount, setMercCount] = useState(0)

  const handleLeadershipChange = (
    valueAsString: string,
    valueAsNumber: number
  ) => setMaxLeadership(valueAsNumber)

  const handleMercCount = () => {
    setMercCount(optimizeBasedOnLeadership(maxLeadership))
  }

  const remainingLeadership = maxLeadership - army.leadership

  return (
    <Box>
      <Heading textAlign="center">Total War Battle Calculator</Heading>

      <Box as="form">
        <VStack spacing={8} direction="column">
          <HStack spacing={10}>
            <Flex>
              <FormControl>
                <FormLabel htmlFor="max-leadership" fontWeight="normal">
                  Max Leadership
                </FormLabel>
                <NumberInput
                  id="max-leadership"
                  maxW="150px"
                  value={maxLeadership}
                  onChange={handleLeadershipChange}
                  min={1}
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
              </FormControl>
            </Flex>
            <Flex>
              <Button onClick={handleMercCount}>Optimal Army</Button>
            </Flex>
          </HStack>

          <Text>
            {mercCount > 0
              ? `Level 7 Epic Monster Hunters: ${Math.floor(mercCount)}`
              : null}
          </Text>

          <Stat>
            <StatLabel>Current Leadership</StatLabel>
            <StatNumber>{army.leadership}</StatNumber>
            <StatHelpText>
              {remainingLeadership == 0 ? null : (
                <StatArrow
                  type={remainingLeadership > 0 ? 'increase' : 'decrease'}
                />
              )}
              {remainingLeadership}
            </StatHelpText>
          </Stat>

          <HStack spacing={24}>
            <FormControl as="legend">
              <FormLabel as="legend">Guardsman</FormLabel>
              <CheckboxGroup
                defaultValue={selectedArmy.guardsman}
                onChange={selectGuardsman}
              >
                <HStack spacing="24px">
                  <Checkbox value="spearman">Spearman</Checkbox>
                  <Checkbox value="archer">Archer</Checkbox>
                  <Checkbox value="rider">Rider</Checkbox>
                  <Checkbox value="griffin">Griffin</Checkbox>
                </HStack>
              </CheckboxGroup>
              <FormHelperText>The more variety the better!</FormHelperText>
            </FormControl>

            <FormControl as="legend">
              <FormLabel as="legend">Epic Monster Hunters</FormLabel>
              <RadioGroup
                value={selectedArmy.epicMH}
                onChange={setEpicMHSelected}
              >
                <HStack spacing="24px">
                  <Radio value="VI">VI</Radio>
                  <Radio value="VII">VII</Radio>
                </HStack>
              </RadioGroup>
              <FormHelperText>Strong against Epics</FormHelperText>
            </FormControl>
          </HStack>
          <HStack>
            <Spearman spearman={army.spearman} setSpearman={setSpearman} />
            <Archer archer={army.archer} setArcher={setArcher} />
            <Rider rider={army.rider} setRider={setRider} />
            <Griffin
              griffin={army.battleGriffin}
              setGriffin={setBattleGriffin}
            />
          </HStack>
        </VStack>
      </Box>
    </Box>
  )
}
