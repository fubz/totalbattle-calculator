import { forEach, mapValues, maxBy, sumBy } from 'lodash'
import { useState, useEffect, useReducer } from 'react'
import { archerStats } from '~/components/guardsmen/Archer'
import { griffinStats } from '~/components/guardsmen/Griffin'
import { riderStats } from '~/components/guardsmen/Rider'
import { spearmanStats } from '~/components/guardsmen/Spearman'

const initialArmyState = {
  leadership: 0,
  spearman: {
    tier1: 0,
    tier2: 0,
    tier3: 0,
    tier4: 0,
    tier5: 0,
  },
  archer: {
    tier1: 0,
    tier2: 0,
    tier3: 0,
    tier4: 0,
    tier5: 0,
  },
  rider: {
    tier1: 0,
    tier2: 0,
    tier3: 0,
    tier4: 0,
    tier5: 0,
  },
  battleGriffin: {
    tier5: 0,
  },
}

const armyStats = {
  spearman: spearmanStats,
  archer: archerStats,
  rider: riderStats,
  battleGriffin: griffinStats,
}

const leadershipStats = {
  spearman: 1,
  archer: 1,
  rider: 2,
  battleGriffin: 20,
}

const emhStats = {
  VI: {
    strength: 2030,
    health: 6090
  },
  VII: {
    strength: 3740,
    health: 11220,
  },
}

export type SpearmanState = {
  tier1: number
  tier2: number
  tier3: number
  tier4: number
  tier5: number
}

export type ArcherState = SpearmanState
export type RiderState = SpearmanState
export type BattleGriffinState = {
  tier5: number
}

export type ArmyState = {
  leadership: number
  spearman: SpearmanState
  archer: ArcherState
  rider: RiderState
  battleGriffin: BattleGriffinState
}

interface SetSpearmanAmountAction {
  type: 'SET_SPEARMAN_AMOUNT'
  tier: TierTypes
  amount: number
}

interface SetArcherAmountAction {
  type: 'SET_ARCHER_AMOUNT'
  tier: TierTypes
  amount: number
}

interface SetRiderAmountAction {
  type: 'SET_RIDER_AMOUNT'
  tier: TierTypes
  amount: number
}

interface SetBattleGriffinAmountAction {
  type: 'SET_BATTLEGRIFFIN_AMOUNT'
  tier: TierTypes
  amount: number
}

interface SetArmy {
  type: 'SET_ARMY'
  army: ArmyState
}

type BattleActions =
  | SetSpearmanAmountAction
  | SetArcherAmountAction
  | SetRiderAmountAction
  | SetBattleGriffinAmountAction
  | SetArmy

type SetSpearman = React.Dispatch<SetSpearmanAmountAction>

export type TierTypes = 'tier1' | 'tier2' | 'tier3' | 'tier4' | 'tier5'

function battleReducer(state: ArmyState, action: BattleActions): ArmyState {
  let newState = state
  switch (action.type) {
    case 'SET_SPEARMAN_AMOUNT':
      newState = {
        ...state,
        spearman: {
          ...state.spearman,
          [action.tier]: action.amount,
        },
      }
      break
    case 'SET_ARCHER_AMOUNT':
      newState = {
        ...state,
        archer: {
          ...state.archer,
          [action.tier]: action.amount,
        },
      }
      break
    case 'SET_RIDER_AMOUNT':
      newState = {
        ...state,
        rider: {
          ...state.rider,
          [action.tier]: action.amount,
        },
      }
      break
    case 'SET_BATTLEGRIFFIN_AMOUNT':
      newState = {
        ...state,
        battleGriffin: {
          ...state.battleGriffin,
          [action.tier]: action.amount,
        },
      }
      break
    case 'SET_ARMY':
      newState = action.army
      break
    default:
      throw new Error(`Unhandled action type: ${JSON.stringify(action)}`)
  }

  newState.leadership = calculateLeadership(newState)

  return newState
}

function calculateLeadership(state: ArmyState): number {
  let newLeadership = 0

  forEach(state, (val, key) => {
    if (['leadership'].includes(key)) {
      return 0
    } else {
      // @ts-ignore
      newLeadership += sumBy(
        // @ts-ignore
        Object.values(state[key]),
        // @ts-ignore
        (amount) => amount * leadershipStats[key]
      )
    }
  })

  return newLeadership
}

interface SetSpearmanSelected {
  type: 'SPEARMAN_SELECTED'
  enabled: boolean
}

interface SetArcherSelected {
  type: 'ARCHER_SELECTED'
  enabled: boolean
}

interface SetRiderSelected {
  type: 'RIDER_SELECTED'
  enabled: boolean
}

interface SetGriffinSelected {
  type: 'GRIFFIN_SELECTED'
  enabled: boolean
}

interface SetEpicMHSelected {
  type: 'EpicMH_SELECTED'
  selected: 'VI' | 'VII'
}

interface SelectGuardsman {
  type: 'SELECT_GUARDSMAN'
  selected: string[]
}

type SelectedActions =
  | SetSpearmanSelected
  | SetArcherSelected
  | SetRiderSelected
  | SetGriffinSelected
  | SetEpicMHSelected
  | SelectGuardsman

const initialSelectedState: SelectedState = {
  spearman: true,
  archer: true,
  rider: true,
  battleGriffin: true,
  guardsman: ['spearman', 'archer', 'rider', 'griffin'],
  epicMH: 'VII',
}

type SelectedState = {
  spearman: boolean
  archer: boolean
  rider: boolean
  battleGriffin: boolean
  guardsman: string[]
  epicMH: 'VI' | 'VII'
}

function selectedReducer(
  state: SelectedState,
  action: SelectedActions
): SelectedState {
  switch (action.type) {
    case 'SELECT_GUARDSMAN':
      return {
        ...state,
        spearman: action.selected.includes('spearman'),
        archer: action.selected.includes('archer'),
        rider: action.selected.includes('rider'),
        battleGriffin: action.selected.includes('griffin'),
        guardsman: action.selected,
      }
    // case 'SPEARMAN_SELECTED':
    //   return {
    //     ...state,
    //     spearman: action.enabled,
    //   }
    // case 'ARCHER_SELECTED':
    //   return {
    //     ...state,
    //     archer: action.enabled,
    //   }
    // case 'RIDER_SELECTED':
    //   return {
    //     ...state,
    //     rider: action.enabled,
    //   }
    // case 'GRIFFIN_SELECTED':
    //   return {
    //     ...state,
    //     battleGriffin: action.enabled,
    //   }
    case 'EpicMH_SELECTED':
      return {
        ...state,
        epicMH: action.selected,
      }
    default:
      throw new Error(`Unhandled action type: ${JSON.stringify(action)}`)
  }
}

export default function useEpicBattle() {
  const [army, dispatch] = useReducer(battleReducer, initialArmyState)
  const [selectedArmy, dispatchSelection] = useReducer(
    selectedReducer,
    initialSelectedState
  )
  console.log({ army, selectedArmy })

  function setSpearman(tier: TierTypes, amount: number) {
    return dispatch({ type: 'SET_SPEARMAN_AMOUNT', tier, amount })
  }

  function setArcher(tier: TierTypes, amount: number) {
    return dispatch({ type: 'SET_ARCHER_AMOUNT', tier, amount })
  }

  function setRider(tier: TierTypes, amount: number) {
    return dispatch({ type: 'SET_RIDER_AMOUNT', tier, amount })
  }

  function setBattleGriffin(tier: TierTypes, amount: number) {
    return dispatch({ type: 'SET_BATTLEGRIFFIN_AMOUNT', tier, amount })
  }

  function optimizeBasedOnLeadership(maxLeadership: number) {
    const base = maxLeadership / selectedArmy.guardsman.length

    const rider = selectedArmy.guardsman.includes('rider') ? base / 2 : 0
    const griffin = selectedArmy.guardsman.includes('griffin') ? base / 20 : 0

    let army = {
      spearman: {
        tier1: 0,
        tier2: 0,
        tier3: 0,
        tier4: 0,
        tier5: selectedArmy.guardsman.includes('spearman')
          ? Math.trunc(base)
          : 0,
      },
      archer: {
        tier1: 0,
        tier2: 0,
        tier3: 0,
        tier4: 0,
        tier5: selectedArmy.guardsman.includes('archer') ? Math.trunc(base) : 0,
      },
      rider: {
        tier1: 0,
        tier2: 0,
        tier3: 0,
        tier4: 0,
        tier5: Math.trunc(rider),
      },
      battleGriffin: {
        tier5: Math.trunc(griffin),
      },
    }

    const strength = mapValues(army, (tiers, soldier) => {
      return mapValues(tiers, (amount, tier) => {
        return amount * armyStats[soldier].tiers[tier].strength
      })
    })

    console.log(JSON.stringify(strength))

    let maxSoldier = ''
    let maxTier = ''
    let maxStrength = -Infinity

    for (let soldier in strength) {
      for (let tier in strength[soldier]) {
        let count = strength[soldier][tier]
        if (count > maxStrength) {
          maxSoldier = soldier
          maxTier = tier
          maxStrength = count
        }
      }
    }

    console.log(`Max: ${maxStrength} ${maxSoldier} ${maxTier}`)

    const mercNumber = maxStrength / emhStats[selectedArmy.epicMH].strength

    dispatch({ type: 'SET_ARMY', army: { ...army, leadership: 0 } })

    return mercNumber
  }

  function setSpearmanSelected(enabled: boolean) {
    return dispatchSelection({
      type: 'SPEARMAN_SELECTED',
      enabled,
    })
  }

  function setArcherSelected(enabled: boolean) {
    return dispatchSelection({
      type: 'ARCHER_SELECTED',
      enabled,
    })
  }

  function setRiderSelected(enabled: boolean) {
    return dispatchSelection({
      type: 'RIDER_SELECTED',
      enabled,
    })
  }

  function setGriffinSelected(enabled: boolean) {
    return dispatchSelection({
      type: 'GRIFFIN_SELECTED',
      enabled,
    })
  }

  function setEpicMHSelected(selected: 'VI' | 'VII') {
    return dispatchSelection({
      type: 'EpicMH_SELECTED',
      selected,
    })
  }

  function selectGuardsman(selected: string[]) {
    return dispatchSelection({
      type: 'SELECT_GUARDSMAN',
      selected,
    })
  }

  return {
    army,
    setSpearman,
    setArcher,
    setRider,
    setBattleGriffin,
    optimizeBasedOnLeadership,
    selectedArmy,
    selectGuardsman,
    setSpearmanSelected,
    setArcherSelected,
    setRiderSelected,
    setGriffinSelected,
    setEpicMHSelected,
  }
}
